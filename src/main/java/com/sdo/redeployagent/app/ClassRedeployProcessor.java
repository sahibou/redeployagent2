package com.sdo.redeployagent.app;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.instrument.ClassDefinition;
import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

public class ClassRedeployProcessor implements Runnable {

	private static final Logger log = Logger.getLogger(ClassRedeployProcessor.class);

	private Instrumentation inst = null;
	private URLClassLoader urlcl = null;

	/**
	 * Sets the Instrumentation for later use.
	 * 
	 * @param inst
	 * @return
	 */
	public ClassRedeployProcessor init(Instrumentation inst) {
		this.inst = inst;
		return this;
	}

	@Override
	public void run() {
		// Classloader
		urlcl = new URLClassLoader(new URL[0]);
log.info("\t\tINITIATE CLASSLOADER 0 !!");
		// Socket to talk to clients
		ZContext context = new ZContext();
		ZMQ.Socket socket = context.createSocket(ZMQ.REP);
		socket.bind("tcp://*:6006");

		while (true) {
			while (!Thread.currentThread().isInterrupted()) {
				byte[] request = socket.recv(0);
				long start = System.currentTimeMillis();

				String request_string = new String(request, ZMQ.CHARSET);
				
				String response = processEvent(request_string) + (System.currentTimeMillis() - start + "ms");
				socket.send(response.getBytes(ZMQ.CHARSET), 0);

				// Thread.sleep(100); // Do some 'work'
			}
		}
	}

	private String processEvent(String reply_string) {
		String action = reply_string.split(";")[0];

		log.info("RECEIVED ["+reply_string+"]");
		if (action.equals("URL"))
			return addClassloaderUrl(reply_string.split(";")[1]);
		else if (action.equals("RED"))
			return redefineClass(reply_string.split(";")[1], reply_string.split(";")[2]);
		else
			return "Action unknown : " + action;
	}

	private String redefineClass(String classFQN, String filePath) {

		try {
			showLoadedUrls();
			log.info("RED");
			log.info("\tFQN:["+classFQN+"]");
			log.info("\tPATH:["+filePath+"]");
			Class<?> c = urlcl.loadClass(classFQN);
			byte[] c_bytes = Files.readAllBytes(Paths.get(filePath));
			ClassDefinition cd = new ClassDefinition(c, c_bytes);
			inst.redefineClasses(cd);
			log.info("\tNew class definition for "+classFQN+" has been loaded");
			return "CLASS redefined ! " + classFQN;
		} catch (Exception x) {
			return getExceptionMessage(x);
		}

	}

	private String addClassloaderUrl(String url) {
		try {
			log.info("URL");
			log.info("\tClassLoader items:["+urlcl.getURLs().length+"]");
			log.info("\tnew URL:["+url+"]");
			if(!Arrays.asList(urlcl.getURLs()).contains(new URL(url))) {
				urlcl = URLClassLoader.newInstance(ArrayUtils.add(urlcl.getURLs(), new URL(url)));
				log.info("\tThis url has been loaded");
			}else {
				log.info("\tThis url already exists");
			}			
			showLoadedUrls();
			return "URLClassloader new paths (" + StringUtils.join(urlcl.getURLs()," & ") + ")";
		} catch (MalformedURLException x) {
			log.error(getExceptionMessage(x	));
			return "Classloader(" + urlcl.getURLs().length + ") failed to load path :" + url;
		}
	}
	
	private String getExceptionMessage(Exception x) {
		x.printStackTrace();
		StringBuilder response = new StringBuilder(x.getMessage());
		log.error(x.getMessage());
		for(StackTraceElement ste : x.getStackTrace()) {
			log.error("\t\t"+ste);
			response.append("\t\t"+ste);			
		}
		return response.toString();
	}
	
	private void showLoadedUrls() {
		log.debug("\tCurrently ["+urlcl.getURLs().length+"] LOADED urls : "+StringUtils.join(urlcl.getURLs()," & "));
	}
}
