package com.sdo.redeployagent.app;

import java.lang.instrument.Instrumentation;

import org.apache.log4j.Logger;

public class ClassRedeployAgentBootstrap {

	private static final Logger log = Logger.getLogger(ClassRedeployAgentBootstrap.class);
 			
	/**
	 * Starts the 0MQ listener thread for class redefinition.
	 * @param agentArgs
	 * @param inst
	 */
	public static void premain(String agentArgs, Instrumentation inst) {
		Thread t = new Thread(new ClassRedeployProcessor().init(inst));
		t.start();
		log.info("#AndWeHaveMECO!! : "+t.getState());
	}

}
